package BotDiscord.event;

import BotDiscord.command.CommandMap;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.events.GenericEvent;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.EventListener;

public class BotListener implements EventListener {
	
	private final CommandMap commandMap;
	
	public BotListener(CommandMap commandMap) {

		this.commandMap = commandMap;
	}

	public void onEvent(GenericEvent event) {
		if (event instanceof MessageReceivedEvent)
			onMessage((MessageReceivedEvent) event);

	}

	private void onMessage(MessageReceivedEvent event) {
		
		
		if (event.getAuthor().equals(event.getJDA().getSelfUser()))
			return;
		/*if (event.getGuild().getSelfMember().hasPermission(Permission.MESSAGE_WRITE)) {
			event.getTextChannel().sendMessage("Bonjour "+event.getAuthor().getAsMention()).queue();

		}*/
		String message = event.getMessage().getContentDisplay();
		if (message.startsWith(commandMap.getTag()) || event.getChannelType().toString() == "PRIVATE") {
			message =message.replaceFirst(commandMap.getTag(), "");
			if (commandMap.commandUser(event.getAuthor(), message, event.getMessage())){
				if(event.getChannelType().toString() == "PRIVATE") return;
				if(event.getTextChannel() != null && event.getGuild().getSelfMember().hasPermission(Permission.MESSAGE_MANAGE)) {
					event.getMessage().delete().queue();
				}
			}
		}
	}
	
	

}
