package BotDiscord.command.defaultc;

import java.awt.Color;
import java.io.IOException;

import ArduinoController.Arduino;

import BotDiscord.BotDiscord;
import BotDiscord.command.Command;
import BotDiscord.command.Command.ExecutorType;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;

public class CommandDefault {
	
	private final BotDiscord botDiscord;
	
	public CommandDefault(BotDiscord botDiscord) {
		this.botDiscord = botDiscord;
	}
	
	@Command(name="stop",type=ExecutorType.CONSOLE)
	private void stop() {
		botDiscord.setRunning(false);
	}
	
	@Command(name="info",type=ExecutorType.USER)
	private void info(User user, MessageChannel channel) {
		if(channel instanceof TextChannel) {
			TextChannel textChannel = (TextChannel)channel;
			if(!textChannel.getGuild().getSelfMember().hasPermission(Permission.MESSAGE_EMBED_LINKS)) return;
			
		}
		EmbedBuilder builder = new EmbedBuilder();
		builder.setAuthor(user.getName());
		builder.setTitle("Informations");
		builder.setDescription("descriptions");
		builder.setColor(Color.ORANGE);
		System.out.println(builder.build());
		channel.sendMessage(builder.build()).queue();
	}
	
	@Command(name="game",type=ExecutorType.ALL)
	private void game(JDA jda, String[] args) {
		StringBuilder builder = new StringBuilder();
		for(String str: args) {
			if(builder.length()>0) builder.append(" ");
			builder.append(str);
		}
		System.out.println();
		jda.getPresence().setActivity(Activity.playing(builder.toString()));
		System.out.println(jda.getPresence().getActivity());
	}
	
	@Command(name="pin13on",type=ExecutorType.USER)
	private void pin13on(User user, MessageChannel channel) {
		try {
			Arduino.setPinValue(botDiscord.getPin(13), 1);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		channel.sendMessage(user.getAsMention()+" vient d'allumer pin13").queue();
	}
	
	@Command(name="pin13off",type=ExecutorType.USER)
	private void pin13off(User user, MessageChannel channel) {
		try {
			Arduino.setPinValue(botDiscord.getPin(13), 0);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		channel.sendMessage(user.getAsMention()+" vient d'eteindre pin13").queue();
	}

}
