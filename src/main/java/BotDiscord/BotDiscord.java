package BotDiscord;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

import javax.security.auth.login.LoginException;

import org.firmata4j.Pin;

import ArduinoController.Arduino;
import BotDiscord.command.CommandMap;
import BotDiscord.event.BotListener;
import net.dv8tion.jda.api.AccountType;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;

public class BotDiscord implements Runnable{
	
	private final JDA jda;
	private final CommandMap commandMap = new CommandMap(this);
	private final Scanner scanner = new Scanner(System.in);
	private boolean running;
	private String token;
	
	interface SetOutput {
        void setThis();
    }
	
	private Pin[] pin = new Pin[14];
	
	public BotDiscord() throws LoginException {
		try {
			File tokenFile = new File("../token.txt");
			FileReader content = new FileReader(tokenFile);
			BufferedReader buffer = new BufferedReader(content); 
			token = buffer.readLine();
			buffer.close();
			
			System.out.println(token);
			
		} catch (IOException e) {
			// TODO: handle exception
			e.printStackTrace();
		} 
		try {
			Arduino.start();
			pin[13] = Arduino.setOutputPin(13);
			Arduino.setPinValue(pin[13], 0);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		jda = new JDABuilder(AccountType.BOT).setToken(token).build();
		jda.addEventListener(new BotListener(commandMap));
	}
	
	public JDA getJda() {
		return jda;
	}
	
	public Pin getPin(int number) {
		return pin[number];
	}
	
	public void setRunning(boolean running) {
		this.running = running;
	}
	

	
	@Override
	public void run() {

		running = true;
		
		while (running) {
			if(scanner.hasNextLine()) {
				commandMap.commandConsole(scanner.nextLine());
			}
		}
		System.out.println("Bot get stop");
		scanner.close();
		System.out.println("Bot is stopped");
		jda.shutdown();
		try {
			Arduino.setPinValue(pin[13], 0);
			Arduino.stop();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		System.exit(0);
		
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			BotDiscord botDiscord = new BotDiscord();
			new Thread(botDiscord, "bot").start();
			System.out.println("bot connected !");
		} catch (Exception e) {
			// TODO: handle exception
		}
		

	}

}
