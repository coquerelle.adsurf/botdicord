package ArduinoController;

import java.io.IOException;

import org.firmata4j.IODevice;
import org.firmata4j.Pin;
import org.firmata4j.firmata.FirmataDevice;

public class Arduino {
	
	private static IODevice device;
	
	public static void start() throws IOException, InterruptedException{
		device = new FirmataDevice("COM3");
		device.start();
		device.ensureInitializationIsDone();
		
	}
	public static Pin setOutputPin(int value) throws IOException, InterruptedException {
		Pin pin = device.getPin(value);
		pin.setMode(Pin.Mode.OUTPUT);
		return pin;
	}
	
	public static Pin setServoPin(int pinValue, int minPulse, int maxPulse) throws IOException, InterruptedException {
		Pin pin = device.getPin(pinValue);
		pin.setMode(Pin.Mode.SERVO);
		pin.setServoMode(minPulse, maxPulse);
		return pin;
	}

	public static void setPinValue(Pin pin, int value) throws IOException, InterruptedException{
		pin.setValue(value);
	}
	
	public static void stop() throws IOException, InterruptedException{
		device.stop();
	}
}
