# Bot Discord #

*En cours de développement...*

Bot Discord en Java permettant de piloter un Arduino via le client firmata4j

Ne pas oublier de renseigner le token via **token.txt**

**BotDiscord.java :** Ne pas oublier **la cohérence du chemin** token.txt à la ligne 33 !

` File tokenFile = new File("../token.txt");`

*Le projet, [inspiré par ce tutoriel](https://www.youtube.com/watch?v=h7hoVYsIZpM), fait avec Maven*

